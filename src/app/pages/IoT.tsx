// Page Components
import '../App.css';

// Images
import picar from '../../resources/complete-built-picar.jpg'
import lab2 from '../../resources/IoT-picar-networking.jpg'


export default function IoT() {

    
    return (
        <div className="main-page" id="iot-page">
            
            <div>
                <h1>Internet of Things</h1>
                <h2>My Robotic Self-Driving Raspberry Pi Car</h2>
                
                <div className='work-content'>
    
                    <div className='work-images'>
                        <img  src={picar} alt="completely built picar for cs437 iot lab 1 project "/>
                        <br/><br/><br/>
                        <img  src={lab2} alt="testing out my client-server app by using it to control my raspberry pi for cs437 lab 2"/>
                        <br/><br/><br/>
                        <iframe className="youtube-videos" src="https://www.youtube.com/embed/1NnPxP_hqM4?si=a3KMt7xjwWBhHFWP" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin" allowFullScreen></iframe>
                    </div>

                    <div className='work-docs'>
                    <iframe title="iot-lab1" className="google-docs" src="https://docs.google.com/document/d/e/2PACX-1vRMJ2xBkuAbGA7-_LwJWMJ3UQG_ae7KZEx3d0bmv5z_gzLHMRqju5La9-1sMxqwI_zipAeIOhJhQ1_B/pub?embedded=true"></iframe>
                    <br/><br/><br/>
                    <iframe title="iot-lab2" className="google-docs" src="https://docs.google.com/document/d/e/2PACX-1vSE6n164XURY4w2pzxGMyMYzec4YEhuERo6Aks_ZvMUQzBLUEcn58RjtFnXXyU5P_jq_F5v4ZrnufzF/pub?embedded=true"></iframe>
                    <br/><br/><br/>
                    <iframe title="iot-lab5" className="google-docs" src="https://docs.google.com/document/d/e/2PACX-1vRojRqe5Y7m88VmrVFAINQK7TYSytY0RzMVksOMAyl9HZZYRsxmfKNZmhu1291Pn6iJ986qD1lzLN22/pub?embedded=true"></iframe>
                    </div>
                </div>
            </div>

            

            
            
        </div>
    )
}


