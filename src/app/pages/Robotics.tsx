// Page Components
import '../App.css';

// Resources
import MyCapstone from '../../resources/aecabrera_parasol_capstone_2023.pdf';

export default function Robotics() {

    
    return (
        <div id="robotics-page">
            
            <div>
                <h1>Robotics: Motion Planning</h1>
                <h2>My Capstone for CS 597</h2>
            
                    
                <iframe id="capstone" src={MyCapstone} title="My Capstone" allow="encrypted-media; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin" allowFullScreen></iframe> 
                    
    
            </div>
        </div>
    )
}


