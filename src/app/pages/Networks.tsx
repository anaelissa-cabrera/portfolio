// Page Components
import '../App.css';

// Images

export default function Networks() {

    
    return (
        <div id="networks-page">
            
            <div>
                <h1>Networks and Security</h1>

                <h2>Network Security for IoT Hardware</h2>

                <div className='work-content'>
    
                    <div className='work-images'>
                    <iframe className="youtube-videos" src="https://www.youtube.com/embed/wu4rzU-ZhpI?si=5guzZL7S9NgxEUIN" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerPolicy="strict-origin-when-cross-origin" allowFullScreen></iframe>
                        <br/><br/><br/>
                        
                    </div>

                    <div className='work-docs'>
                        <iframe title="iot-lab3" id="docs-lab3" src="https://docs.google.com/document/d/e/2PACX-1vTfgI_EG37fHe7_fkcgmERsIS7HrbjxEcLe1yZSPidWM4dF6sk9xthd6BVl_YAPn_6BeVl9fYjeSniw/pub?embedded=true"></iframe>
                        
                    </div>

                    
                </div>

                    
                
                        
                    
            </div>
        </div>

            

            
            
    )
}


