// Page Components
import '../App.css';
import { Link } from 'react-router-dom';

// Media
// import Rain from '../../resources/rainycity.mp4';
import Robot from '../../resources/covervid.mp4'
import Selfie from '../../resources/me-chicago.jpg';
import FullStack from '../../resources/nasa-logo.png';
import IoT from '../../resources/my-fritzing-blueprint.png';
import Network from '../../resources/network.png';
import Robotics from '../../resources/motion-planning.png';
import Programming from '../../resources/programming.png';


export default function HomeView() {

    return (
        <div>
            <div className="main-page" id="home-page">
                
                {/** BACKGROUND VIDEO COVER */}
                <div id="text-picture-overlay">
                    <div>
                        <video id="cover-video" loop autoPlay muted>
                            <source src={Robot} type="video/mp4" />
                        </video>
                        <p className='overlay'>
                            Resourceful. <br/>
                            Resilient. <br/>
                            Meticulous. <br/>
                            Driven. <br/>
                            Creative.<br/>
                            Problem-Solver.
                        </p>
                    </div>
                    <div>
                        <img id="selfie" src={Selfie} alt="ana elissa has dark brown hair, brown eyes, fair skin"/>
                    </div>
                        
                    
                </div>

                {/** ABOUT ME */}
                <div className="home-section" id="about">
                    <h1 className="section-titles">ABOUT ME</h1>
                    <p>
                        Ana Elissa Cabrera is a Computer Scientist that specializes in 
                        back-end programming  with advanced proficiency in Python, Java, and 
                        TypeScript. At the core of her specialty is an extensive understanding of 
                        Algorithms and Data Structures. Ana Elissa also has research 
                        experience at the Parasol Laboratory where she was advised by renowed 
                        roboticist Dr. Nancy M. Amato and mentored by recent doctorate graduate 
                        Dr. Diane Uwacu. Ana Elissa's research was on motion planning and experimenting with 
                        robots with multiple degrees of freedom in Eucledian spaces.<br/><br/>

                        Ana Elissa's education history is unique given her charge to pursue her dream 
                        of becoming a computer scientist soon after she graduated college during
                        the pandemic in the early 2020s. After she received her Bachelor's of Science in Political 
                        Science with a focus on international relation and a minor in Linguistics
                        from the University of Illinois at Urbana-Champaign, she enrolled in the iCAN
                        program where she obtained a post-graduate degree in Computer Science, which 
                        allowed her to catch up on all her computer science classes at Illinois to 
                        start her Master's degree in Computer Science, part-time and online, with 
                        an expected graduation date of May 2026.<br/><br/>
                        
                        Ana Elissa is currently seeking a long-term full-time position or internship
                        in the fields of software engineering, technical consulting, patent/intellectual 
                        property analyst, and information technology. She hopes to give her technical expertise, 
                        intrepersonal skills, foreign language skills, and wisdom acquired from her 
                        international and national globstudies, multi-faceted life experiences as an asset to a team that 
                        can help her grow, learn, and develop as a young professional.
                    </p>
                </div>   

                {/** PROFESSIONAL EXPERIENCE */}
                <div className="home-section" id="profesh">
                    <h1 className="section-titles">PROFESSIONAL EXPERIENCE</h1>

                    <div className="job-exp">
                        <h4>Protivit - Anti-Money Laundering (AML) Analyst (Summer 2024)</h4>
                        <p>
                            <ul>
                            <li> Applied pattern recognition, advanced Excel skills, and AML expertise 
                                to detect unusual financial activity in big datasets for a top 2 US financial institution</li><br/>

                            <li> Performed data analytics on spreadsheets and researched customer 
                                digital footprints via open-source tools for over 75 flagged business 
                                and consumer accounts; reviewed 25 potentially-suspicious cases, 
                                authored and submitted 54 non-suspicious reports to the Financial Crimes Unit. </li><br/>
                            </ul>
                        </p>
                    </div>

                    • • • • •

                    <div className="job-exp">
                        <h4>Protivit - IT Customer Operations Representative Project (Summer 2024)</h4>
                        <p>
                            <ul>
                            <li> Monitored and reported technical account errors in 3 CRM databases to 
                                ensure smooth transition in a large bank’s software migration project</li><br/>

                            <li> Assisted over 300 customers, ages 18 to 93 years, on how to access and 
                                navigate the new User Interface (UI) applications, and analyzed customer
                                opinions on our mobile and website applications to gauge customers' 
                                user experience (UX) </li><br/>

                            <li> Taught vulnerable demographics basic security skills such and phishing 
                                awareness as well as resources on how to better protect their banking data </li><br/>

                            <li> Resolved 15 to 30 customer calls per day for seven weeks, totaling about 
                                700 calls, on topics ranging from data migration to password resets </li><br/>
                            </ul>
                        </p>
                    </div>

                    • • • • • 

                    <div className="job-exp">
                        <h4>Target - Team Member (March 2022 to September 2022)</h4>
                        <p>
                            <ul>
                                <li>Greeted customers and provided guidance and information on general and specialty merchandise.</li><br/>

                                <li>Answered phone calls to help guests with general questions or redirect them to correct departments</li><br/>
                                <li>Managed strategic product placements based on item popularity and store hot-spots to increase sales</li><br/>
                                <li>Fulfilled online order pickups (OPUs), assisted guests with OPU check-ins and delivered their orders</li><br/>
                                <li>Recorded inventory audits, pulled items from inventory and set up push flow carts, completed one-for-one pulls</li><br/>
                                <li>Pushed products to sales floor, and stocked excess items into inventory to maintain store isles orderly stocked</li><br/>
                            </ul>
                        </p>
                    </div>
                </div> 
                • • • • • 
                    
                {/** PROJECTS */}
                <div className="home-section" id="projects">

                    <h1 className="section-titles">PROJECTS</h1>
                    <div className="columns">
                        <div> &#8205; &#8205; &#8205;</div>
                        <br/><br/>

                        <div className="skills">
                            <img className="icon" src={FullStack} alt="TBA"/>
                            <p> 
                                <span className="full-stack">FULL-STACK</span> <br/><br/>
                                {/**<Link to="/*"><button id="academicBtn">Learn More</button></Link>*/}
                                <button id="academicBtn"><a href="https://anaelissa-cabrera.gitlab.io/mp2">Learn More</a></button>
                            </p>
                            <br/><br/>
                        </div>

                        <div className="skills">
                            <img className="icon" src={IoT} alt="my fritzing blueprint of my non-holonomic robotic car"/>
                            <p>
                            <span className="iot"> INTERNET OF THINGS</span><br/><br/>
                            <Link to="/iot"><button id="profeshBtn">Learn More</button></Link>
                            </p>
                        </div>



                        <div className="skills">
                            <img src={Network} alt="website in progress" className="icon"/>
                            <p>
                            <span className="ido-title"> NETWORKS </span><br/><br/>
                            <Link to="/networks"><button id="personalBtn">Learn More</button></Link>
                            </p>
                        </div> 

                        <div className="skills">
                            <img src={Robotics} alt="website in progress" className="icon"/>
                            <p>
                            <span className="ido-title"> ROBOTICS </span><br/><br/>
                            <Link to="/robotics"><button id="personalBtn">Learn More</button></Link>
                            </p>
                        </div> 

                        <div className="skills">
                            <img src={Programming} alt="website in progress" className="icon"/>
                            <p>
                            <span className="ido-title"> PROGRAMMING </span><br/><br/>
                            <button id="personalBtn">TBA...</button>
                            </p>
                        </div> 
                    </div>  
                </div>

            </div>
        </div>
            
    )
}

