import './App.css';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import HomeView from './pages/Home';
import MusicPlayer from '../components/MusicPlayer';

import IoT from './pages/IoT';
import Networks from './pages/Networks';
import Navbar from '../components/HomeNavbar';
import Footer from '../components/HomeFootbar';
import Robotics from './pages/Robotics';

function App()  {

  return (
    <Router>
      <div>
        <Navbar />
      <Routes>
            <Route index element={<HomeView />}/>
              <Route path="/home" element={<HomeView />} />
              <Route path="/iot" element={<IoT/>} />
              <Route path="/networks" element={<Networks />} />
              <Route path="/robotics" element={<Robotics />} />
              <Route path="/*" element={<HomeView />} />
              {/*<Route path="/*" element={<NoMatch />} />*/}
      </Routes>
      <MusicPlayer />
      
      </div>
      <Footer />
    </Router>
  );
}


export default App;
