export default function FilterSort() {
    
    // Today's Date
    const date = new Date();
    const todaysDate = new Date(date).toISOString().split('T')[0]

    return (
        <div id="filter-sort">
            {/* */}

            {/* FILTER */}
            <div id="filter">
                
                {/* query by start_date */}
                <form>
                    <label>Filter by Start Date &#8205;</label>
                        <input type="date" value="2024-03-07" min="2024-03-07" className="start-end" name="start"/>
                        <input type="submit"/>
                </form>
                <p>&#8205; &#8205;&#8205; &#8205;</p>
                {/* query by end_date */}
                <form>
                    <label> End Date &#8205; </label>
                        <input type="date" value={todaysDate} className="start-end" name="end" max={todaysDate}/>
                        <input type="submit"/>
                </form>
            
            
                <p>&#8205; &#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;&#8205;</p>
                {/* query by count */}
                <form action="./NoMatch.tsx">
                    <label> &#8205;&#8205;&#8205;|&#8205;&#8205; &#8205;Filter by Count: </label>
                    {/* Shows first 25 items */}
                    <input type="radio" id="25" name="show-count" value={25}/>
                        <label>25</label>

                    {/* Shows first 50 items */}
                    <input type="radio" id="50" name="show-count" value={50}/>
                        <label>50</label>

                    {/* Shows first 100 items */}
                    <input type="radio" id="100" name="show-count" value={100}/>
                        <label>100</label>
                    <input type="submit" value="Submit"/>
                </form> 
                

            {/* SORT */}
            </div>
            <div id="sort">
                <form action="./NoMatch.tsx">
                    <label>Sort by: </label>
                    <select name="sort" id="old-new">
                        <option value="sort-new">Date (New to Old)</option>
                        <option value="sort-old">Date (Old to New)</option>
                    </select>
                    <input type="submit" value="Submit"/>
                </form>
            </div>
        </div>
    )
}

