import { Link } from 'react-router-dom';


export default function NavBar() {

    const homeNav = {
        background: 'black'
        // background: '#2C041C' //wine
        // background: '#0C2D48' // blue
        // background: '#5B3256' // purple
    }

    return (
        <nav style={homeNav}>
            <div className='explore'> 
                <div> &#8205; &#8205; &#8205;</div>
            </div>

            <div className='home-logo'>
                <h2>ANA ELISSA CABRERA</h2>
            </div>

            <div className='links' id="home-links">
            <Link to="/home">Home &#8205; &#8205; &#8205; &#8205;</Link>
            <a href="https://anaelissa-cabrera.gitlab.io/mp2">Full-Stack &#8205; &#8205; &#8205; &#8205;</a>
            <Link to="/iot">IoT &#8205; &#8205; &#8205; &#8205;</Link>
            <Link to="/networks">Networks &#8205; &#8205; &#8205; &#8205;</Link>
            <Link to="/robotics">Robotics &#8205; &#8205; &#8205; &#8205;</Link>
            </div>

    </nav>

)
}

