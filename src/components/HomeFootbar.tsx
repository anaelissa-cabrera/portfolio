import React from 'react';

// Resources
import ViewResume from '../resources/resume-01.jpeg';
import Resume from '../resources/anaelissa-cabrera-resume-internship.pdf';

export default function Footer() {

    const homeFooter = {
        background: 'black'
    }

return(
    <div>
        <div className="footer" style={homeFooter}>
                <div>
                <br/>
                <h3>CONTACT</h3>
                Ana Elissa Cabrera <br/>
                email: aec4@illinois.edu <br/>
                
                <br/>
            </div>

            <div className="footer-block">
                <br/>
                <h3>Let's Connect!</h3>
        
                <div className="icons">
                <a href="https://www.linkedin.com/in/anaelissa-cabrera/">
                    <img className="socials-button" alt="link to my linkedIn account"
                    src="https://static.vecteezy.com/system/resources/previews/018/930/587/original/linkedin-logo-linkedin-icon-transparent-free-png.png"/>
                </a>
            
                <a href="https://www.youtube.com/channel/UCe75mYHGoW3aMtvm7BELXCw/videos">
                    <img className="socials-button" alt="link to my youtube account"
                    src="https://static.vecteezy.com/system/resources/previews/018/930/572/original/youtube-logo-youtube-icon-transparent-free-png.png"/>
                </a>

                <a href="https://gitlab.com/anaelissa-cabrera">
                    <img className="socials-button" alt="link to my gitlab account"
                    src="https://pbs.twimg.com/profile_images/1526219998741647362/8KKDKESj_400x400.jpg"/>
                </a>
                
                <a href="https://github.com/anaelissa-cabrera">
                    <img className="socials-button" alt="link to my github account"
                    src="https://www.pngmart.com/files/23/Github-Logo-PNG.png"/>
                </a>
                </div>
            </div>

                <div>
                    <br/>
                    <h3>Resume</h3>
                    <a href={Resume} target="_blank" rel="noreferrer">
                        <img id="footer-resume-button" src={ViewResume} alt="resume document with pink butterflies"/>
                    </a>
                </div>


            </div>
    </div>
)
}
