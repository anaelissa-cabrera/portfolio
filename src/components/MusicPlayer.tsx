import React, { Component } from "react";

// Page Components
import '../app/App.css';

// Media
import BackgroundMusic from '../resources/kingdomhearts-utada-passion.mp3';
import PlayMusic from '../resources/chocobo.jpeg';

class MusicPlayer extends Component {
	// Create state
	state = {

		// Get audio file in a variable
		audio: new Audio(BackgroundMusic),

		// Set initial state of song
		isPlaying: false,
	};

	// Main function to handle both play and pause operations
	playPause = () => {

		// Get state of song
		let isPlaying = this.state.isPlaying;

		if (isPlaying) {
			// Pause the song if it is playing
			this.state.audio.pause();
		} else {

			// Play the song if it is paused
			this.state.audio.play();
		}

		// Change the state of song
		this.setState({ isPlaying: !isPlaying });
	};

	render() {
		return (
			<div>

				
				<button id="play-pause" onClick={this.playPause}>
                    <img id="music-icon" src={PlayMusic} alt="classy piano in cozy room with rainyindow background"/>
                </button>

                {/* Alert users  */}
                <p id="music-status">
                {this.state.isPlaying ?
						"Ⅱ Pause" :
						"▷ Play"}
                </p>

			</div>
		);
	}
}

export default MusicPlayer;
